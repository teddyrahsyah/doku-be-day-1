import java.util.Scanner;

public class BilanganPrima {
    public static void main(String[] args) {
        //
        Scanner myInput = new Scanner( System.in );

        int N = myInput.nextInt();
        boolean isNotPrime = false;

        if (N > 1) {
            for (int i = 2; i<N; i++) {
                if (N % i == 0) {
                    isNotPrime = true;
                    break;
                }
            }
        }

        if (isNotPrime) {
            System.out.println("Bukan Bilangan Prima");
        } else {
            System.out.println("Bilangan Prima");
        }
    }
}