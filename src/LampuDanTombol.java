import java.util.Scanner;

public class LampuDanTombol {
    public static void main(String[] args) {

        Scanner myInput = new Scanner( System.in );

        int N = myInput.nextInt();
        boolean lightOn = false;

        for (int i = 1; i <= N; i++) {
            if (N % i == 0) {
                lightOn = !lightOn;
            }
        }

        if (lightOn) {
            System.out.println("lampu menyala");
        } else {
            System.out.println("lampu mati");
        }
    }
}
